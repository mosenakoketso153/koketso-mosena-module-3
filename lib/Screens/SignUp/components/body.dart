import 'package:flutter/material.dart';
import 'package:flutter_application_1/Components/already_having_an_account.dart';
import 'package:flutter_application_1/Components/rounded_button.dart';
import 'package:flutter_application_1/Components/rounded_input_field.dart';
import 'package:flutter_application_1/Components/rounded_password_field.dart';
import 'package:flutter_application_1/Screens/Home/home_Screem.dart';
import 'package:flutter_application_1/Screens/Login/login_screen.dart';
import 'package:flutter_application_1/Screens/SignUp/components/background.dart';
//import 'package:flutter_application_1/Screens/SignUp/components/or_divider.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Body extends StatelessWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              "Create Account ",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SvgPicture.asset(
              "assets/Icons/VarsityAlly1.svg",
              height: size.height * 0.35,
            ),
            RoundedInputField(
                hintText: "First Name",
                icon: Icons.person,
                onchanged: (value) {}),
            RoundedInputField(
                hintText: "Last Name",
                icon: Icons.person,
                onchanged: (value) {}),
            RoundedInputField(
                hintText: "Email", icon: Icons.email, onchanged: (value) {}),
            RoundedPasswordField(onChanged: (value) {}),
            RoundedPasswordField(onChanged: (value) {}),
            RoundedButton(
              text: "Register",
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (contet) {
                      return (const Home());
                    },
                  ),
                );
              },
            ),
            AlreadyHaveAnAccountCHeck(
                login: false,
                press: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return (const LoginScreen());
                  }));
                }),
            // const OrDivider(),
          ],
        ),
      ),
    );
  }
}
