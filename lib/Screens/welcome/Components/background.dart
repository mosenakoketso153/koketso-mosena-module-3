import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  final Widget child;
  const Background({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // ignore: sized_box_for_whitespace
    return Container(
      height: size.height,
      width: double.infinity,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            bottom: -1,
            right: -1,
            width: size.width * 0.3,
            child: Image.asset("assets/images/VarsityAlly (2).png"),
          ),
          Positioned(
            bottom: -1,
            left: -1,
            width: size.width * 0.5,
            child: Image.asset("assets/images/VarsityAlly2 (2).png"),
          ),
          child,
        ],
      ),
    );
  }
}
