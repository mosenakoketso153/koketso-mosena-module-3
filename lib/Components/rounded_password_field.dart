import 'package:flutter/material.dart';
import 'package:flutter_application_1/Components/text_field_container.dart';

class RoundedPasswordField extends StatelessWidget {
  final ValueChanged<String> onChanged;
  const RoundedPasswordField({
    Key? key,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
        child: TextField(
      obscureText: true,
      onChanged: onChanged,
      decoration: const InputDecoration(
          hintText: "Enter Your Password",
          icon: Icon(
            Icons.lock,
            color: Color.fromRGBO(9, 100, 216, 1),
          ),
          suffixIcon: Icon(
            Icons.visibility,
            color: Color.fromRGBO(9, 100, 216, 1),
          ),
          border: InputBorder.none),
    ));
  }
}
