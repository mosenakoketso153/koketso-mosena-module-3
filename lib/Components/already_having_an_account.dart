import 'package:flutter/material.dart';

class AlreadyHaveAnAccountCHeck extends StatelessWidget {
  final bool login;
  final Function press;
  const AlreadyHaveAnAccountCHeck({
    Key? key,
    required this.login,
    required this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          login ? "Don't have an Account? " : "ALready Have an Accounr? ",
          style: const TextStyle(color: Color.fromRGBO(9, 100, 216, 1)),
        ),
        GestureDetector(
          onTap: press as Function(),
          child: Text(login ? "Register Here" : "Sign In",
              style: const TextStyle(fontWeight: FontWeight.bold)),
        ),
      ],
    );
  }
}
