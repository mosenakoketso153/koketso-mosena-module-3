import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  final Widget child;
  const Background({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            bottom: 0,
            child: Image.asset(
              "assets/images/VarsityAlly (5).png",
              width: size.width * 0.45,
            ),
          ),
          Positioned(
            top: 34,
            right: 0,
            child: Image.asset(
              "assets/images/VarsityAlly (6).png",
              width: size.width * 0.35,
            ),
          ),
          child,
        ],
      ),
    );
  }
}
