// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final String text;
  final Function? press;
  final Color color, textcolor;
  const RoundedButton({
    Key? key,
    required this.text,
    required this.press,
    this.color = const Color.fromRGBO(9, 100, 216, 1),
    this.textcolor = Colors.black,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // ignore: avoid_unnecessary_containers
    return Container(
      width: size.width * 0.8,
      // ignore: prefer_const_constructors
      margin: EdgeInsets.symmetric(vertical: 8),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: FlatButton(
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 60),
          color: color,
          onPressed: press as void Function()?,
          child: Text(text),
        ),
      ),
    );
  }
}
