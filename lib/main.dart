import 'package:flutter/material.dart';
import 'package:flutter_application_1/Screens/welcome/welcome_Screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'VarsityAlly',
      theme: ThemeData(
        // ignore: prefer_const_constructors
        primaryColor: Color.fromARGB(120, 0, 174, 255),
        scaffoldBackgroundColor: Colors.white,
      ),
      home: welcomeScreen(),
    );
  }
}
