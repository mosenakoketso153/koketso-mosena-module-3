// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_application_1/Components/already_having_an_account.dart';
import 'package:flutter_application_1/Components/rounded_button.dart';
import 'package:flutter_application_1/Components/rounded_input_field.dart';
import 'package:flutter_application_1/Components/rounded_password_field.dart';
import 'package:flutter_application_1/Screens/Home/home_Screem.dart';
import 'package:flutter_application_1/Screens/Login/components/background.dart';
import 'package:flutter_application_1/Screens/SignUp/signup_screen.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Body extends StatelessWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              "LOGIN",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: size.height * 0.05,
            ),
            SvgPicture.asset(
              "assets/Icons/VarsityAlly.svg",
              height: size.height * 0.35,
            ),
            RoundedInputField(
              hintText: "Enter Your Email",
              onchanged: (value) {},
              icon: Icons.email,
            ),
            RoundedPasswordField(
              onChanged: (value) {},
            ),
            RoundedButton(
              text: "LOGIN",
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return (Home());
                    },
                  ),
                );
              },
            ),
            AlreadyHaveAnAccountCHeck(
              login: true,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return (SignUpScreen());
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
