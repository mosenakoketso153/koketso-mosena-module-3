import 'package:flutter/material.dart';

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),

      width: size.width * 0.9,
      // ignore: prefer_const_constructors
      decoration: BoxDecoration(
        color: const Color.fromRGBO(1, 1, 1, 0),
        border: Border.all(color: const Color.fromRGBO(0, 30, 164, 1)),
        borderRadius: BorderRadius.circular(10),
      ),
      child: child,
    );
  }
}
