import 'package:flutter/material.dart';
import 'package:flutter_application_1/Components/text_field_container.dart';

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onchanged;
  const RoundedInputField({
    Key? key,
    required this.hintText,
    required this.icon,
    required this.onchanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        onChanged: onchanged,
        decoration: InputDecoration(
            icon: Icon(
              icon,
              color: const Color.fromRGBO(9, 100, 216, 1),
            ),
            hintText: hintText,
            border: InputBorder.none),
      ),
    );
  }
}
