import 'package:flutter/material.dart';
import 'package:flutter_application_1/Screens/Home/edit_profile.dart';
import 'package:flutter_application_1/Screens/Home/home_Screem.dart';
import 'package:flutter_application_1/Screens/Login/login_screen.dart';

class Winning extends StatelessWidget {
  const Winning({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
        TextButton.styleFrom(primary: Theme.of(context).colorScheme.onPrimary);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Winning Apps"),
        actions: <Widget>[
          TextButton(
            style: style,
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return (const Edit());
              }));
            },
            child: const Text('Edit Profile'),
          ),
          TextButton(
            style: style,
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return (const Home());
              }));
            },
            child: const Text('Home'),
          ),
          IconButton(
            icon: const Icon(Icons.notifications),
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                content: Text("This a SanckBar"),
              ));
            },
          ),
        ],
      ),
      body: const Center(
        child: Text(
          "Winning Apps",
          style: TextStyle(fontSize: 30),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return (const LoginScreen());
              },
            ),
          );
        },
        label: const Text("Logout"),
        icon: const Icon(Icons.logout),
        backgroundColor: Colors.blue,
      ),
    );
  }
}
